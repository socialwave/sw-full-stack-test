# Teste Full Stack Social Wave Ruby On Rails

### Descriao da aplicação
A aplicação tem como objetivo a criação e venda de ingressos, e deve utilizar as seguintes tecnologias : 
* [Ruby On Rails](http://api.rubyonrails.org/) 
* [Jquery](https://jquery.com/) 
* [Html, Css](https://www.w3schools.com/) 
* [MySql](https://www.mysql.com/) 
### Requisitos da aplicação
A aplicação deve permitir a criação, atualização, exclusão e visualização utilizando os padrões 
estabelecidos pelo framework Ruby On Rails.

* Os dados devem ser persistidos em um banco de dados.

* Um comprador deve poder comprar ingressos.

* Todos os campos dos ingressos e do pedido são necessários.

* Devem ser listados para venda somente os ingressos que tiverem data >= a data atual.

### Objetos da Aplicação

A aplicação deverá conter os seguintes objetos: 
* Ingresso 
  * Preço(float)
  * Nome(string)
  * Taxa(float)
  * Data(datetime)
* Comprador 
  * Nome(string)
  * Email(string)
  * Telefone(float)
* Pedido 
  * * Comprador
  * Ingresso
  * pago(boolean)

### Coisas que queremos ver

* Código limpo e legível
* Utilização das funcionalidades e geradores do Rails
* Projeto que respeite a arquitetura do Rails

### Diferenciais

* Telas bonitas(utilizando bootstrap, preferencialmente)
* Validações Inteligentes
* Utilização correta do ActiveModel
* Versionamento GIT
* Novas Funcionalidades 

### Considerações
O teste deverá ser enviado no formato ZIP ou com o link para o repositório GIT para o email: ricardo@socialwave.com.br.


